require 'minitest/autorun'
require 'calculator'

describe Calculator, "Test Calculator" do
  before do
    @calc = Calculator.new
  end

  it 'add two positive numbers' do
    assert_equal 6, @calc.add(2,4) 
  end

  it 'test subtract two positive numbers' do
    assert_equal 1, @calc.subtract(3,2)
  end

  it 'test memory' do
    @calculator.memory = 2
    assert_equal 2, @calculator.memory
  end
end
